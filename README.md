# pa_LTL_module
This a pulseadio module to intake from a audio source feed to a look to listen neural net and output to an audio sink.


Resources:
* [Paper](https://arxiv.org/abs/1804.03619)
* [video about paper](https://www.youtube.com/watch?v=zL6ltnSKf9k)
* [Code base for NN](https://github.com/crystal-method/Looking-to-Listen)

Datasets:
* https://looking-to-listen.github.io/avspeech/explore.html


Depencies:
* NUMPY https://pypi.org/project/numpy/
* opencv-python https://pypi.org/project/opencv-python/
* pyaudio https://pypi.org/project/PyAudio/
    * setuptools https://pypi.org/project/setuptools/
    * portaudio19-dev


Install for dev (expected Ubuntu 18.04 & 19.04):
```
##FIXME show each method for target distros
apt install portaudio19-dev
git clone https://gitlab.com/Andruid/pa_ltl_module.git
cd pa_ltl_module
python3 -m pip install numpy
python3 -m pip install setuptools
python3 -m pip install opencv-python
```

Install for operation:
```
apt install portaudio19-dev
git clone https://gitlab.com/Andruid/pa_ltl_module.git
cd pa_ltl_module
python3 -m pip install numpy
python3 -m pip install setuptools
python3 -m pip install opencv-python
```