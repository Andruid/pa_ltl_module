#!/usr/bin/env python3

import pyaudio
import cv2
import look_to_listen as ltl
import logging
import numpy as np
import time

## Logging
logging.basicConfig(level='INFO')
## Audio Defaults
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
CHUNK = 1024
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = "pa_LTL_test_audio.wav"
## Video Defaults
###FIXME replace with a way to select a device or default to 0
video_capture_device = 0

class VideoCapture(object):
    def __init__(self, device_num):
        self.VideoObj = cv2.VideoCapture(device_num)
    def __enter__(self):
        return self.VideoObj
    def __exit__(self, type, value, traceback):
        self.VideoObj.release()

#with cv2.VideoCapture(video_capture_device) as video_data:
logging.info('Connecting to video input')
VideoObj = cv2.VideoCapture(video_capture_device)

audio = pyaudio.PyAudio()

logging.info('Connecting to audio input')
inputAudioStream = audio.open(format=FORMAT, channels=CHANNELS,
                rate=RATE, input=True,
                frames_per_buffer=CHUNK)

logging.info('Connecting to audio output')
outputAudioStream = audio.open(format=FORMAT, channels=CHANNELS,
                rate=RATE, output=True,
                frames_per_buffer=CHUNK)

def format_audio(audio_stream):
    frames = []
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = audio_stream.read(CHUNK)
        frames.append(data)
    formated_audio = frames

    return formated_audio

def format_video(VideoObj):
    frames = []
    retlist = []
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        ret, frame = VideoObj.read(CHUNK)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #retlist.append(ret)
        frames.append(gray)
    formated_video_frames = frames
    return formated_video_frames

def send_data_to_LTL(video_input, audio_input):
    '''Sends audio and video from camera to look_to_listen'''
    logging.DEBUG'send_data_to_LTL called')
    video_frames = format_video(video_input)
    audio_frames = format_audio(audio_input)
    isolated_audio = ltl.proccess_streams(video_frames, audio_frames)
    return isolated_audio

def stop_recording(stream):
    stream.stop_stream()
    stream.close()
    audio.terminate()

def test_audio(input, output):
    import wave
    print("recording...")
    audio_frames = format_audio(input)
    print("finished recording")
    stop_recording(input)
    stop_recording(output)
    waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    waveFile.setnchannels(CHANNELS)
    waveFile.setsampwidth(audio.get_sample_size(FORMAT))
    waveFile.setframerate(RATE)
    waveFile.writeframes(b''.join(audio_frames))
    waveFile.close()
    print('wavefile: {WAVE_OUTPUT_FILENAME} was made')

def test_video(VideoObj):
    video_frames = format_video(VideoObj)
    print('Opening frame viewer')
    for frame in video_frames:
        cv2.imshow('frame',frame)
        cv2.waitKey(1)
    print('Closing frame viewer')
    VideoObj.release()

def test_all(**args):
    print('start testing')
    test_audio(inputAudioStream, outputAudioStream)
    test_video(VideoObj)
    print('done testing')
    exit()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('Creates isolated audio based on video and audio inputs')
    parser.add_argument('T', help='Run test to insure audio and video input are correct',
                        action=test_all)
    while True:
        isolated_audio = send_data_to_LTL(VideoObj, inputAudioStream)
        outputAudioStream.write(isolated_audio)



